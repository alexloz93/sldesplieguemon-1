# Monitorización y loggin sobre OCP 3.9
# Santalucía

En el presente repositorio se recogen los entregables correspondientes a los entregables de los diferentes hitos.

## Esquema inicial del repositorio
El esquema indicado a continuación puede sufrir modificaciones a lo largo del desarrollo por necesidades específicas.

*-Documentation* (Carpeta donde se alojan los "manuales" necesarios para algún despliegue junto con todos los adjuntos originales)

*-metrics_logging* (Carpeta donde se alojan los ficheros correspondientes a los entregables)

*--hito 1*

*--hito 2*

*--hito 3*

*--hito 4*

*--hito 5*

*--hito 6*

*-tools* (Alojamiento para las herramientas que sean necesarias desplegar para la correcta exportación de métricas y logs)

## Estructura en el mensaje del commit

**feat:** nueva funcionalidad

**fix:** corrección de un bug

**docs:** cambios de documentación

**style:** cambios estéticos

**refactor:** cambios que ni es un bug ni una nueva funcionalidad

**chore:** cambios de mantenimiento que no afectan al código fuente

**revert:** revertir un commit previo
